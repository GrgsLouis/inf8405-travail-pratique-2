package com.example.tp2.dataclass;

import java.io.Serializable;

public class BluetoothCustomDevice implements Serializable {

    private String name;
    private String latitude;
    private String longitude;
    private String macAddress;
    private int type;
    private boolean favorite;

    public BluetoothCustomDevice(String name, String macAddress, String latitude, String longitude, int type, boolean favorite) {
        super();
        this.name = name;
        this.macAddress = macAddress;
        this.longitude = longitude;
        this.type = type;
        this.latitude = latitude;
        this.favorite = favorite;
    }

    public String getName() {
        return name;
    }

    public int getType() {
        return type;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public boolean getFavorite() {
        return favorite;
    }
}