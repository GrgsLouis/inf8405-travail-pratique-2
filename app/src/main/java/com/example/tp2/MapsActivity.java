package com.example.tp2;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import com.example.tp2.dataclass.BluetoothCustomDevice;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static android.content.Intent.*;
import static com.example.tp2.Constants.ERROR_DIALOG_REQUEST;
import static com.example.tp2.Constants.PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION;
import static com.example.tp2.Constants.PERMISSIONS_REQUEST_ENABLE_GPS;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, LocationListener {

    private static final String FILE_NAME = "devicesList.txt";
    private static final String TAG = "TAG";

    private GoogleMap mMap;
    private Button themeButton;
    private LatLng myLocation;

    private ListView lstvw;
    private ArrayAdapter aAdapter;
    private static final int FINE_LOCATION_PERMISSION = 200;
    private List<Double> listLatitudePositions = new ArrayList<Double>();
    private List<Double> listLongitudePositions = new ArrayList<Double>();
    ArrayList list = new ArrayList();
    private BluetoothAdapter bAdapter = BluetoothAdapter.getDefaultAdapter();

    private LocationManager locationManager;
    private LocationListener locationListener;
    private double latitude, longitude;
    private boolean mLocationPermissionGranted = false;
    private FusedLocationProviderClient mFusedLocationClient;

    public int i = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        Button getPairedDevices = (Button) findViewById(R.id.getPairedDevices);
        lstvw = (ListView) findViewById(R.id.deviceList);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        getPairedDevices.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bAdapter == null) {
                    Toast.makeText(getApplicationContext(), "Bluetooth Not Supported", Toast.LENGTH_SHORT).show();
                } else {
                    Set<BluetoothDevice> pairedDevices = bAdapter.getBondedDevices();
                    if (!bAdapter.isDiscovering()) {
                        checkLocationPermission();
                        bAdapter.startDiscovery();
                        IntentFilter discoverDevicesIntent = new IntentFilter(BluetoothDevice.ACTION_FOUND);
                        registerReceiver(discoverDevicesReceiver, discoverDevicesIntent);
                    }
                    aAdapter = new ArrayAdapter(getApplicationContext(), android.R.layout.simple_list_item_1, list);
                    lstvw.setAdapter(aAdapter);
                }
            }
        });

        themeButton = (Button) findViewById(R.id.Theme);
        themeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int theme;
                if (i == 4) {
                    i = 1;
                    theme = R.raw.aubergine;
                    changeTheme(theme);
                } else if (i == 1) {
                    theme = R.raw.dark;
                    changeTheme(theme);
                    i++;
                } else if (i == 2) {
                    theme = R.raw.aubergine;
                    changeTheme(theme);
                    i++;
                } else if (i == 3) {
                    theme = R.raw.night;
                    changeTheme(theme);
                    i++;
                }
            }
        });

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);


        //Uncomment this to delete the text file when the application is run.
//        try {
//            this.eraseTextFile();
//        } catch (Exception e) {
//            e.getStackTrace();
//        }

        if (fileExists(FILE_NAME)){
            this.load();
        }
        //this method will initiate all the permissions methods for google map.
        if (checkMapServices()) {
            if (mLocationPermissionGranted) {
                //TODO use the application as intended.
                //Nothing to do for now
                getLastKnownLocation();
            } else {
                getLocationPermission();
            }
        }

        //TODO Splash Screen

    }

    private void getLastKnownLocation() {
        Log.d(TAG, "getLastKnownLocation: called");
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mFusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            myLocation = new LatLng(location.getLatitude(), location.getLongitude());
                            Log.d(TAG,"GetLastLocation Latitude : " + myLocation.latitude);
                            Log.d(TAG,"GetLastLocation Longitutde : " + myLocation.longitude);
                            mMap.addMarker(new MarkerOptions().position(myLocation).title("MyLocation")).showInfoWindow();
                            mMap.moveCamera(CameraUpdateFactory.newLatLng(myLocation));
                        }
                    }
                });
    }

    private void changeTheme(int theme) {
        mMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, theme));
    }

    protected void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    FINE_LOCATION_PERMISSION);
        }
    }

    private final BroadcastReceiver discoverDevicesReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "WE ARE IN THE BLUETOOTH");
            final String action = intent.getAction();
            if (action.equals(BluetoothDevice.ACTION_FOUND)) {
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                BluetoothCustomDevice customDevice = new BluetoothCustomDevice(device.getName(), device.getAddress(), String.valueOf(latitude),
                        String.valueOf(longitude), device.getType(), false);
                String output = "Name: " + customDevice.getName() + " DeviceType: " + String.valueOf(customDevice.getType()) + " MAC Address: " + customDevice.getMacAddress() +
                        " Latitude: " + customDevice.getLatitude() + " Longitude: " + customDevice.getLongitude() + " Favorite: " + customDevice.getFavorite();

                Log.d(TAG, "customDevice string latitude: " + customDevice.getLatitude());
                Log.d(TAG, "customDevice string longitude: " + customDevice.getLongitude());
                double testLatitude = Double.parseDouble(customDevice.getLatitude());
                double testLongitude = Double.parseDouble(customDevice.getLongitude());
                if (list.contains(output) == false) {
                    try {
                        list.add(output);
                        listLatitudePositions.add(Double.parseDouble(customDevice.getLatitude()));
                        listLongitudePositions.add(Double.parseDouble(customDevice.getLongitude()));
                        Log.d(TAG, "customeDevice double latitude: " + String.valueOf(testLatitude));
                        Log.d(TAG, "customeDevice double longitude: " + String.valueOf(testLongitude));
                        save(output);
                    } catch (Exception e) {
                        e.getStackTrace();
                    }
                }
                //TODO ecrire devicename et macAddress dans un fichier et location (LatLng)
                //TODO assurer que ya pas de doublon dans le fichier, verifier si existe, adapter load et write du fichier


                aAdapter = new ArrayAdapter(getApplicationContext(), android.R.layout.simple_list_item_1, list);
                lstvw.setAdapter(aAdapter);
                placeMarkersOnMap();
//                unregisterReceiver(discoverDevicesReceiver);
            }

        }
    };

    /*
     *
     * THIS BLOCK OF CODE IS FOR GOOGLE MAPS FUNCTIONS
     *
     * */
    //this method is responsable for determining if the device
    //is able to use google services
    public boolean isServicesOK() {
        Log.d(TAG, "isServicesOK: checking google services version");

        int available = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(MapsActivity.this);

        if (available == ConnectionResult.SUCCESS) {
            //everything is fine and the user can make map requests
            Log.d(TAG, "isServicesOK: Google Play Services is working");
            return true;
        } else if (GoogleApiAvailability.getInstance().isUserResolvableError(available)) {
            //an error occured but we can resolve it
            Log.d(TAG, "isServicesOK: an error occured but we can fix it");
            Dialog dialog = GoogleApiAvailability.getInstance().getErrorDialog(MapsActivity.this, available, ERROR_DIALOG_REQUEST);
            dialog.show();
        } else {
            Toast.makeText(this, "You can't make map requests", Toast.LENGTH_SHORT).show();
        }
        return false;
    }

    //is gps enabled or not
    public boolean isMapsEnabled() {
        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();
            return false;
        }
        return true;

    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("This application requires GPS to work properly, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        Intent enableGpsIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivityForResult(enableGpsIntent, PERMISSIONS_REQUEST_ENABLE_GPS);
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult: called.");
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ENABLE_GPS: {
                if (mLocationPermissionGranted) {
                    //TODO use the application as intended.
                    //Nothing to do for now
                    getLastKnownLocation();
                } else {
                    getLocationPermission();
                }
            }
        }

    }

    private void getLocationPermission() {
        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         */
        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true;
            //TODO use the application as intended.
            //Nothing to do for now
            getLastKnownLocation();
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        mLocationPermissionGranted = false;
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mLocationPermissionGranted = true;
                }
            }
        }
    }

    private boolean checkMapServices() {
        if (isServicesOK()) {
            if (isMapsEnabled()) {
                return true;
            }
        }
        return false;
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            boolean success = googleMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            this, R.raw.aubergine));
            if (!success) {
                Log.e("MapsActivity", "Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e("MapsActivity", "Can't find style. Error: ", e);
        }
        // Add a marker to our location
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        //Blue dot on the map that shows my location
        googleMap.setMyLocationEnabled(true);
        placeMarkersOnMap();
        listenOnMarkerClick();

    }

    @Override
    public void onLocationChanged(@NonNull Location location) {
        latitude = location.getLatitude();
        longitude = location.getLongitude();
    }

    /*
    *
    * THIS BLOCK OF CODE IS FOR SAVING AND LOADING AND DELETING FROM A FILE
    *
    * */
    public void save(String output) throws FileNotFoundException {
        Log.d("Output value: ", output);
        FileOutputStream fos = null;
        String eof = "\n";
        try {
            fos = openFileOutput(FILE_NAME, MODE_APPEND);
            fos.write(output.getBytes());
            fos.write(eof.getBytes());

            Log.d("Save output: ",  output);
            Toast.makeText(this, "Saved to " + getFilesDir() + "/" + FILE_NAME,
                    Toast.LENGTH_LONG).show();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    public void load() {
            FileInputStream fis = null;
            try {
                fis = openFileInput(FILE_NAME);
                InputStreamReader isr = new InputStreamReader(fis);
                BufferedReader br = new BufferedReader(isr);
                String text;
                while ((text = br.readLine()) != null) {
                    extractLocations(text);
                    list.add(text);
                    Log.d("Text value: ", text);
                }
                aAdapter = new ArrayAdapter(getApplicationContext(), android.R.layout.simple_list_item_1, list);
                lstvw.setAdapter(aAdapter);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (fis != null) {
                    try {
                        fis.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
    }

    public void eraseTextFile() throws IOException {
        Log.d(TAG, "eraseTextFile");
        File dir = getFilesDir();
        File file = new File(dir, FILE_NAME);
        boolean deleted = file.delete();
    }

    public boolean fileExists(String filename) {
        File file = this.getFileStreamPath(filename);
        if(file == null || !file.exists()) {
            return false;
        }
        return true;
    }

    /*This function is not currently working
    * Scanner lance une erreure
    * Donc elle va juste dans le catch et elle ne fait pas le try*/
    public void extractLocations(String text){
        /*example of a text that we receive in parameter
        *
        * Name: gDevice-beacon DeviceType: 2 MAC Address: BE:AC:10:00:00:02 Latitude: 37.421998333333335 Longitude: -122.084 Favorite: false
        *
        * We have to extract 37.421998333333335 and -122.084
        * and convert them to double with only 2 decimal number
        * */

        //Adding all the latitude that we already found in the listLatitudePositions array.
        int indexOfLatitude = text.indexOf("Latitude");
        int indexOfLatitudeStart = indexOfLatitude + 10;
        String latitudeValueInString = text.substring(indexOfLatitudeStart, indexOfLatitudeStart+18);
        double lati = Double.parseDouble(latitudeValueInString);
        Log.d(TAG, "The value of latitude should be: " + String.valueOf(lati));
        listLatitudePositions.add(lati);

        //Adding all the longitude that we already found in the listLongitudePositions array.
        int indexOfLongitude = text.indexOf("Longitude");
        int indexOfLongitudeStart = indexOfLongitude + 11;

        String longitudeValueInString = text.substring(indexOfLongitudeStart, indexOfLongitudeStart+8);
        double longi = Double.parseDouble(longitudeValueInString);
        Log.d(TAG, "The value of longitude should be: " + String.valueOf(longi));
        listLongitudePositions.add(longi);
    }

    public void placeMarkersOnMap(){
        if(listLongitudePositions.size() == listLatitudePositions.size()){
            LatLng location = new LatLng(0,0);
            for (int i=0; i< listLongitudePositions.size(); i++){
                location = new LatLng(listLatitudePositions.get(i), listLongitudePositions.get(i));
                mMap.addMarker(new MarkerOptions().position(location).title("Device"+String.valueOf(i))).showInfoWindow();
            }
        }
    }

    public void listenOnMarkerClick(){
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                calculateDirections(marker);
                return false;
            }
        });
    }

    public void calculateDirections(Marker marker){
        Uri gmmIntentUri = Uri.parse("google.navigation:q="+String.valueOf(marker.getPosition().latitude)+","+String.valueOf(marker.getPosition().longitude)+"&mode=d");
        Intent mapIntent = new Intent(ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        startActivity(mapIntent);
    }

}